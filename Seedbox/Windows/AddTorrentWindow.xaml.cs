﻿using Seedbox.Services;
using Seedbox.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Seedbox.Windows
{
    public partial class AddTorrentWindow : Window
    {
        AddTorrentViewModel viewModel;

        public AddTorrentWindow()
        {
            InitializeComponent();

            viewModel = new AddTorrentViewModel(this);
            DataContext = viewModel;

            KeyDown += AddTorrentWindow_KeyDown;
        }

        private void AddTorrentWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void PathTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && viewModel.AddCanExecute())
                viewModel.AddExecute();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
             if (e.Key == Key.Escape)
                Close();
        }
    }
}
