﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Seedbox.Windows
{
    public partial class MessageWindow : Window
    {
        public MessageWindow()
        {
            InitializeComponent();

            KeyDown += MessageWindow_KeyDown;
        }

        private void MessageWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        public static void Show(string message)
        {
            var window = new MessageWindow();
            window.MessageTextBlock.Text = message;
            window.ShowDialog();
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
