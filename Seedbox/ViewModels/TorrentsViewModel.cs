﻿using Seedbox.Helpers;
using Seedbox.Models;
using Seedbox.Services;
using Seedbox.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Seedbox.ViewModels
{
    public class TorrentsViewModel : BaseViewModel
    {
        HttpService http;
        SettingsService settings;
        bool clipboardCanAddTorrent = true;
        bool restarting;

        public ObservableCollection<Torrent> Torrents { get; private set; }

        private Torrent selectedTorrent;
        public Torrent SelectedTorrent { get => selectedTorrent; set => OnPropertyChanged(ref selectedTorrent, value); }

        private bool error;
        public bool Error { get => error; set => OnPropertyChanged(ref error, value); }

        private bool loaded;
        public bool Loaded { get => loaded; set => OnPropertyChanged(ref loaded, value); }

        public bool IsShowNoTorrents { get => Torrents.Count == 0 && Loaded && !Error; }

        public ICommand DisconnectCommand { get; private set; }
        public ICommand AddTorrentCommand { get; private set; }
        public ICommand RemoveTorrentCommand { get; private set; }
        public ICommand RestartCommand { get; private set; }
        public ICommand ClipboardTorrentCommand { get; private set; }

        static bool alreadyAdded;

        public TorrentsViewModel()
        {
            http = App.HttpService;
            settings = App.SettingsService;

            Torrents = new ObservableCollection<Torrent>();

            Task.Run(GetTorrentsAsync);

            DisconnectCommand = new RelayCommand(DisconnectExecute);
            AddTorrentCommand = new RelayCommand(AddTorrentExecute, () => !Error);
            RemoveTorrentCommand = new RelayCommand(RemoveTorrentExecute, RemoveTorrentCanExecute);
            RestartCommand = new RelayCommand(RestartExecute, () => !restarting);
            ClipboardTorrentCommand = new RelayCommand(ClipboardAddTorrentExecute, ClipboardAddTorrentCanExecute);

            if (!alreadyAdded)
            {
                var args = Environment.GetCommandLineArgs();
                if (args.Length > 1)
                    Task.Run(() => AddTorrentAsync(args[1]));
                alreadyAdded = true;
            }
        }

        async Task GetTorrentsAsync()
        {
            while (true)
            {
                var torrents = await http.GetTorrentsAsync();

                Error = torrents == null ? true : false;
                Loaded = true;
                if (Error)
                    await Application.Current.Dispatcher.InvokeAsync(Torrents.Clear);
                else
                {
                    await Application.Current.Dispatcher.InvokeAsync(() =>
                    {
                        foreach (var torrent in torrents)
                        {
                            var torrentObservable = Torrents.SingleOrDefault(t => t.Hash == torrent.Hash);
                            if (torrentObservable == null)
                                Torrents.Add(torrent);
                            else
                            {
                                torrentObservable.Name = torrent.Name;
                                torrentObservable.Size = torrent.Size;
                                torrentObservable.Percent = torrent.Percent;
                                torrentObservable.Download = torrent.Download;
                                torrentObservable.Send = torrent.Send;
                                torrentObservable.Ratio = torrent.Ratio;
                                torrentObservable.Reception = torrent.Reception;
                                torrentObservable.TimeLeft = torrent.TimeLeft;
                            }
                        }

                        if (Torrents.Count != torrents.Count)
                        {
                            for (int i = 0; i < Torrents.Count; i++)
                            {
                                if (!torrents.Contains(Torrents[i]))
                                {
                                    Torrents.RemoveAt(i);
                                    break;
                                }
                            }
                        }
                    });
                }

                OnPropertyChanged(nameof(Torrents));
                await Utility.RaiseCanExecuteChangedAsync();
                OnPropertyChanged(nameof(IsShowNoTorrents));
            }
        }

        void AddTorrentExecute()
        {
            var window = new AddTorrentWindow();
            window.ShowDialog();
        }

        public async Task<bool> AddTorrentAsync(string path)
        {
            Torrent torrent = null;

            clipboardCanAddTorrent = false;
            await Utility.RaiseCanExecuteChangedAsync();

            if (path.EndsWith(AddTorrentViewModel.torrentCheck) && File.Exists(path))
                torrent = await http.AddFileTorrentAsync(path);
            else if (path.StartsWith(AddTorrentViewModel.magnetCheck))
                torrent = await http.AddMagnetTorrentAsync(path);

            clipboardCanAddTorrent = true;
            await Utility.RaiseCanExecuteChangedAsync();

            if (torrent != null)
            {
                await Application.Current.Dispatcher.InvokeAsync(() => Torrents.Add(torrent));
                OnPropertyChanged(nameof(Torrents));
                OnPropertyChanged(nameof(IsShowNoTorrents));

                return true;
            }
            else
                await Application.Current.Dispatcher.InvokeAsync(() => MessageWindow.Show("Torrent non ajouté"));

            return false;
        }

        public void RemoveTorrentExecute()
        {
            if (ConfirmWindow.Show("Confirmer la suppression ?"))
            {
                if (SelectedTorrent == null)
                    return;

                var torrent = Torrents.Single(t => t.Hash == SelectedTorrent.Hash);

                Task.Run(async () =>
                {
                    await http.RemoveTorrentAsync(torrent.Hash);

                    await Application.Current.Dispatcher.InvokeAsync(() => Torrents.Remove(torrent));
                    OnPropertyChanged(nameof(Torrents));
                    await Utility.RaiseCanExecuteChangedAsync();
                    OnPropertyChanged(nameof(IsShowNoTorrents));
                });
            }
        }

        public bool RemoveTorrentCanExecute()
        {
            return !Error && SelectedTorrent != null;
        }

        void DisconnectExecute()
        {
            http.Disconnect();
            settings.Authorization = string.Empty;
            settings.Write();
            MainWindowViewModel.instance.CurrentView = new LoginViewModel();
            MainWindowViewModel.instance.UpdateTitle(null);
        }

        public void ClipboardAddTorrentExecute()
        {
            if (Clipboard.ContainsData(DataFormats.FileDrop))
            {
                var path = Clipboard.GetData(DataFormats.FileDrop) as string[];
                Task.Run(async () => await AddTorrentAsync(path[0]));
            }
            else if (Clipboard.ContainsText())
            {
                var text = Clipboard.GetText();
                Task.Run(async () => await AddTorrentAsync(text));
            }
        }

        public bool ClipboardAddTorrentCanExecute()
        {
            if (Error || !clipboardCanAddTorrent)
                return false;
            if (Clipboard.ContainsData(DataFormats.FileDrop) || Clipboard.ContainsText())
                return true;

            return false;
        }

        void RestartExecute()
        {
            restarting = true;
            Task.Run(async () =>
            {
                await http.RestartAsync();
                restarting = false;
                await Utility.RaiseCanExecuteChangedAsync();
            });
        }
    }
}
