﻿using Microsoft.Win32;
using Seedbox.Helpers;
using Seedbox.Models;
using Seedbox.Services;
using Seedbox.Windows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Seedbox.ViewModels
{
    public class AddTorrentViewModel : BaseViewModel
    {
        public static readonly string torrentCheck = ".torrent";
        public static readonly string magnetCheck = "magnet:";

        AddTorrentWindow window;
        HttpService http;

        string path;
        public string Path { get => path; set => OnPropertyChanged(ref path, value); }
       
        private bool adding;
        public bool Adding { get => adding; set => OnPropertyChanged(ref adding, value); }

        public ICommand CloseCommand { get; private set; }
        public ICommand BrowseCommand { get; private set; }
        public ICommand AddCommand { get; private set; }

        public AddTorrentViewModel(AddTorrentWindow window)
        {
            this.window = window;
            http = App.HttpService;

            CloseCommand = new RelayCommand(window.Close);
            BrowseCommand = new RelayCommand(BrowseExecute, () => !Adding);
            AddCommand = new RelayCommand(AddExecute, AddCanExecute);
        }

        void BrowseExecute()
        {
            var dialog = new OpenFileDialog();
            dialog.Title = "Choisir un torrent";
            dialog.Filter = "Fichier torrent (*.torrent)|*.torrent";

            if (dialog.ShowDialog() == true)
                Path = dialog.FileName;
            window.PathTextBox.Focus();
        }

        public void AddExecute()
        {
            Task.Run(async () =>
            {
                Adding = true;
                await Utility.RaiseCanExecuteChangedAsync();

                var torrentsViewModel = MainWindowViewModel.instance.CurrentView as TorrentsViewModel;
                if (await torrentsViewModel.AddTorrentAsync(Path))
                    await Application.Current.Dispatcher.InvokeAsync(window.Close);

                Adding = false;
                await Utility.RaiseCanExecuteChangedAsync();
            });
        }

        public bool AddCanExecute()
        {
            if (string.IsNullOrEmpty(Path) || Adding)
                return false;

            if (Path.StartsWith(magnetCheck) || Path.EndsWith(torrentCheck))
                return true;

            return false;
        }
    }
}
