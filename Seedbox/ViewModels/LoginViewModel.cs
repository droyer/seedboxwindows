﻿using Seedbox.Helpers;
using Seedbox.Models;
using Seedbox.Services;
using Seedbox.Views;
using Seedbox.Windows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace Seedbox.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private const string buttonMainText = "Se connecter";
        private const string buttonLoggingText = "Connexion...";

        HttpService http;
        SettingsService settings;

        private bool logging;
        public bool Logging { get => logging; set => OnPropertyChanged(ref logging, value); }

        private string username;
        public string Username { get => username; set => OnPropertyChanged(ref username, value); }

        private string buttonText;
        public string ButtonText { get => buttonText; set => OnPropertyChanged(ref buttonText, value); }

        public ICommand ConnectCommand { get; private set; }

        public LoginViewModel()
        {
            http = App.HttpService;
            settings = App.SettingsService;

            buttonText = buttonMainText;

            ConnectCommand = new RelayCommand<PasswordBox>(ConnectExecute, ConnectCanExecute);
        }

        public void CheckLogin(PasswordBox passwordBox)
        {
            Task.Run(async () =>
            {
                if (!string.IsNullOrEmpty(settings.Authorization))
                {
                    Logging = true;
                    ButtonText = buttonLoggingText;

                    try
                    {
                        var decode = Utility.Base64Decode(settings.Authorization);
                        if (decode.Contains(':'))
                        {
                            var tab = decode.Split(':');
                            Username = tab[0];
                            await Application.Current.Dispatcher.InvokeAsync(() => passwordBox.Password = tab[1]);
                        }
                    }
                    catch { }

                    var logged = await http.LoginAsync(settings.Authorization);

                    if (!logged)
                    {
                        await Application.Current.Dispatcher.InvokeAsync(() => MessageWindow.Show("Impossible de se reconnecter"));
                        settings.Authorization = string.Empty;
                        settings.Write();
                    }
                    else
                        MainWindowViewModel.instance.UpdateTitle(settings.Authorization);

                    if (logged)
                        MainWindowViewModel.instance.CurrentView = new TorrentsViewModel();

                    Logging = false;
                    ButtonText = buttonMainText;
                }
            });
        }

        void ConnectExecute(PasswordBox passwordBox)
        {
            Logging = true;
            Utility.RaiseCanExecuteChanged();
            settings.Authorization = Utility.Base64Encode($"{Username}:{passwordBox.Password}");

            Task.Run(async () =>
            {
                ButtonText = buttonLoggingText;
                var logged = await http.LoginAsync(settings.Authorization);
                if (logged)
                {
                    settings.Write();
                    MainWindowViewModel.instance.UpdateTitle(settings.Authorization);
                    MainWindowViewModel.instance.CurrentView = new TorrentsViewModel();
                }
                else
                    await Application.Current.Dispatcher.InvokeAsync(() => MessageWindow.Show("Impossible de se connecter"));

                Logging = false;
                ButtonText = buttonMainText;
            });
        }

        bool ConnectCanExecute(PasswordBox passwordBox)
        {
            if (Logging)
                return false;

            if (string.IsNullOrWhiteSpace(Username))
                return false;

            if (string.IsNullOrWhiteSpace(passwordBox.Password))
                return false;

            return true;
        }
    }
}
