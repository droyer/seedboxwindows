﻿using Seedbox.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Seedbox.Views
{
    public partial class TorrentsView : UserControl
    {
        TorrentsViewModel viewModel;

        static bool once = true;

        public TorrentsView()
        {
            InitializeComponent();

            Loaded += TorrentsView_Loaded;
        }

        private void TorrentsView_Loaded(object sender, RoutedEventArgs e)
        {
            viewModel = DataContext as TorrentsViewModel;

            if (once)
            {
                var window = Window.GetWindow(this);
                window.KeyDown += Window_KeyDown;
                once = false;
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.V && Keyboard.Modifiers == ModifierKeys.Control && viewModel.ClipboardAddTorrentCanExecute())
                viewModel.ClipboardAddTorrentExecute();
        }

        private void Grid_Drop(object sender, DragEventArgs e)
        {
            var torrentsViewModel = DataContext as TorrentsViewModel;

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                Task.Run(() => torrentsViewModel.AddTorrentAsync(files[0]));
            }
        }

        private void ListView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && viewModel.RemoveTorrentCanExecute())
                viewModel.RemoveTorrentExecute();
        }
    }
}
